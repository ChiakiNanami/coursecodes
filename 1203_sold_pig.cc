#include<iostream>
#include<vector>
#include<queue>
#include<stack>

using namespace std;

const int inf = 0x3f3f3f3f;


int farms[1024];
vector<int> vis(1024, -1);
vector<vector<int>> graph(105, vector<int>(105, 0));

vector<int> previous(105);

void augment(int s, int t)
{
    int current = t;
    int pre = previous[current];
    int bottleneck = inf;
    while (pre != -1)
    {
        bottleneck = bottleneck < graph[pre][current] ? bottleneck : graph[pre][current];
        current = pre;
        pre = previous[current];
    }

    current = t;
    pre = previous[current];
    while (pre != -1)
    {
        graph[pre][current] -= bottleneck;
        graph[current][pre] += bottleneck;
        current = pre;
        pre = previous[current];
    }
}

bool maxFlowBFS(int s, int t)
{
    for (int i = 0; i < 105; ++i)
    {
        previous[i] = -1;
    }

    queue<int> que;
    bool visited[105] = {0};
    que.push(s);
    while (!que.empty())
    {
        int next = que.front();
        que.pop();
        visited[next] = true;

        if (next == t)
            return true;

        for (int j = 0; j <= t; ++j)
        {
            if (graph[next][j] == 0)
            {
                continue;
            }
            if (!visited[j])
            {
                previous[j] = next;
                que.push(j);
            }
        }
    }
    return false;
}

int maxFlowEK(int s, int t)
{
    for (int i = 0; i <= t; ++i)
    {
        for (int j = 0; j <= t; ++j)
        {
            graph[i][j] = graph[i][j];
        }
    }
    while (maxFlowBFS(s, t))
    {
        augment(s, t);
    }

    int maxFlow = 0;
    for (int i = 0; i <= t; ++i)
    {
        // flows out of source is flow into source in augGraph
        maxFlow += graph[i][s];
    }

    return maxFlow;
}

int main()
{    
    int numOfFarms, numOfCustomers;

    // input
    cin >> numOfFarms >> numOfCustomers;
    int s = 0;
    int t = numOfCustomers + 1;
    // init farms
    for (int i = 1; i <= numOfFarms; ++i)
    {
        cin >> farms[i];
    }
    // init graph
    for (int currentCustomer = 1; currentCustomer <= numOfCustomers; ++currentCustomer)
    {
        // i = 1 : numOfCustomers
        int numOfKeys;
        cin >> numOfKeys;
        for (int k = 0; k < numOfKeys; ++k)
        {
            int key;
            cin >> key;
            if (vis[key] == -1)
            {
                vis[key] = currentCustomer;
                graph[s][currentCustomer] += farms[key];
            }
            else
            {
                int previousCustomer = vis[key];
                vis[key] = currentCustomer;
                graph[previousCustomer][currentCustomer] = inf;
            }
        }
        int requirement;
        cin >> requirement;
        // link customers to sinks
        graph[currentCustomer][t] = requirement;
    }

    cout << maxFlowEK(s, t);

    return 0;
}