#include<iostream>
#include<vector>
#include<queue>
#include<stack>
#include<cstring>

using namespace std;

const long long inf = 0xffffffff;  
vector<vector<long long>> graph(105,vector<long long>(105, 0));
vector<int> previous(105);
int v, e;



void augment(int s, int t)
{
    int current = t;
    int pre = previous[current];
    long long n = inf;
    while (pre != -1)
    {
        n = n < graph[pre][current] ? n : graph[pre][current];
        current = pre;
        pre = previous[current];
    }

    current = t;
    pre = previous[current];
    while (pre != -1)
    {
        graph[pre][current] -= n;
        graph[current][pre] += n;
        current = pre;
        pre = previous[current];
    }
}

bool maxFlowBFS(int s, int t)
{
    for (int i = 0; i < 105; ++i)
    {
        previous[i] = -1;
    }

    queue<int> que;
    vector<bool> visited(105, 0);
    que.push(s);
    while (!que.empty())
    {
        int next = que.front();
        que.pop();
        visited[next] = true;

        if (next == t)
            return true;

        for (int j = 1; j <= v; ++j)
        {
            if (graph[next][j] == 0)
            {
                continue;
            }
            if (!visited[j])
            {
                previous[j] = next;
                que.push(j);
            }
        }
    }
    return false;
}

long long maxFlowEK(int s, int t)
{

    while (maxFlowBFS(s, t))
    {
        augment(s, t);
    }

    long long maxFlow = 0;
    for (int i = 1; i <= v; ++i)
    {
        maxFlow += graph[i][s];
    }

    return maxFlow;
}

int main()
{
    int cnt = 0;
    int source,sink;
    cin >> v >> e >> source >>sink;
    for (int i = 0; i< e; ++i)
    {
        int a,b;
        long long w;
        cin >>a >>b >>w;
        graph[a][b] += w; //有重边
    }
    cout << maxFlowEK(source, sink);
    return 0;
}