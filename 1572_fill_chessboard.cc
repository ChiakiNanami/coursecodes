#include<iostream>
#include<vector>
#include<queue>
#include<stack>
#include<cstring>

using namespace std;

// 最大二分匹配:匈牙利算法
bool board[35][35];
int m, n;
int dx[4] = {-1,0,1,0}, dy[4] = {0,1,0,-1};
bool broken[35][35];
pair<int, int> match[35][35];

bool find(int x, int y)
{
    for(int i = 0; i< 4; ++i)
    {
        int a = x + dx[i], b= y + dy[i];
        if (a >= 1 && a <= m && b>=1 && b<=n &&broken[a][b] && !board[a][b])
        {
            board[a][b] = 1;
            pair<int,int> t = match[a][b];
            if(t.first == -1 || find(t.first, t.second))
            {
                match[a][b] = {x,y};
                //cout<< t.first<<t.second<<' ' << a<<b<<' '<<x<<y<<endl;
                return true;
            }
        }
    }
    return false;
}

bool chessboard()
{
    int ans = 0;
    memset(match, -1, sizeof match);
    memset(broken, 1, sizeof broken);
    int k;
    cin >>m >>n >>k;
    int x, y;
    for (int i = 0; i < k; ++i)
    {
        cin>> x >> y;
        broken[x][y] = 0;
    }
    for(int i = 1; i<=m; ++i)
    {
        for(int j =1; j <=n; ++j)
        {
            //cout<<i<<j << broken[i][j]<<endl;
            if((i+j)%2==1 &&broken[i][j])
            {
                memset(board, 0, sizeof board);
                if(find(i,j)){
                //cout<< i<<j<<endl;
                    ans += 2;
                }
            }
        }
    }
    //cout << ans <<endl;
    return ans == (m*n -k);
}


int main()
{
    int T;
    cin >> T;
    for(int i = 0; i< T; ++i)
    {
        if(chessboard())
        {
            cout<<"YES"<<endl;
        }
        else
            cout << "NO"<<endl;
    }
    return 0;
}